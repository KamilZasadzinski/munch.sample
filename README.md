* clone repo

* cd to /munch.sample file and start a server (ex: http-server)

* page: http://127.0.0.1:8080/sample_uxdoc_mapping/#/workshop/july (domain, port 可能不同)

* 主要修改這兩頁

```
#!html
/sample_uxdoc_mapping/modules/workshop/july.html
/sample_uxdoc_mapping/modules/workshop/SampleWorkshopController.js

```

* Typescript complier 要開

* 最後額外步驟需要加上 modal的 html template, 記得要放絕對網址

```
#!html
/sample_uxdoc_mapping/modules/workshop/template.shopInfo.html
```
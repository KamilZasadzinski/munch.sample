var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Example;
        (function (Example) {
            angular.module('NineYi.Sample.Example', [
                'NineYi.FrontEnd.Munch',
                'NineYi.Sample.Example.Controllers',
                'NineYi.Sample.Example.Configs'
            ]);
            angular.module('NineYi.Sample.Example.Controllers', []);
            angular.module('NineYi.Sample.Example.Configs', ['pascalprecht.translate']);
        })(Example = Sample.Example || (Sample.Example = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=app.sample.example.js.map
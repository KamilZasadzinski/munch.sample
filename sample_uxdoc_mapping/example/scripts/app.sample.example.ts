module NineYi.Sample.Example {
    angular.module('NineYi.Sample.Example', [
        'NineYi.FrontEnd.Munch',
        'NineYi.Sample.Example.Controllers',
        'NineYi.Sample.Example.Configs']);

    angular.module('NineYi.Sample.Example.Controllers', []);
    angular.module('NineYi.Sample.Example.Configs', ['pascalprecht.translate']);
}
module NineYi.Sample.Services {
    export class TableDataService {
        static $inject = ['$http', '$log', '$q','$window'];

        public api:string = "/sample_uxdoc_mapping/services/";

        constructor(private $http:ng.IHttpService,
                    private $log:ng.ILogService,
                    private $q:ng.IQService,
                    private $window:ng.IWindowService) {
            if($window.location.origin.indexOf('127.0.0.1')<0){
                this.api = `/nineyi.frontend.munch${this.api}`;
            }
        }

        GetSimpleTableData():any {
            var defer = this.$q.defer();
            var url = `${this.api}SimpleData.json`;

            this.$http.get(url)
                .success((result:any) => {
                    defer.resolve(result);
                })
                .error(defer.reject);

            return defer.promise;
        }

        GetGroupedOrderTableData():any {
            var defer = this.$q.defer();
            var url = `${this.api}GroupedOrderTableData.json`;

            this.$http.get(url)
                .success((result:any) => {
                    defer.resolve(result);
                })
                .error(defer.reject);

            return defer.promise;
        }

        GetGroupCategoryTableData():any {
            var defer = this.$q.defer();
            var url = `${this.api}GroupCategoryTableData.json`;

            this.$http.get(url)
                .success((result:any) => {
                    defer.resolve(result);
                })
                .error(defer.reject);

            return defer.promise;
        }
    }
    angular.module('NineYi.Sample.Services')
        .service('TableDataService', TableDataService);
}
var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Services;
        (function (Services) {
            var TableDataService = (function () {
                function TableDataService($http, $log, $q, $window) {
                    this.$http = $http;
                    this.$log = $log;
                    this.$q = $q;
                    this.$window = $window;
                    this.api = "/sample_uxdoc_mapping/services/";
                    if ($window.location.origin.indexOf('127.0.0.1') < 0) {
                        this.api = "/nineyi.frontend.munch" + this.api;
                    }
                }
                TableDataService.prototype.GetSimpleTableData = function () {
                    var defer = this.$q.defer();
                    var url = this.api + "SimpleData.json";
                    this.$http.get(url)
                        .success(function (result) {
                        defer.resolve(result);
                    })
                        .error(defer.reject);
                    return defer.promise;
                };
                TableDataService.prototype.GetGroupedOrderTableData = function () {
                    var defer = this.$q.defer();
                    var url = this.api + "GroupedOrderTableData.json";
                    this.$http.get(url)
                        .success(function (result) {
                        defer.resolve(result);
                    })
                        .error(defer.reject);
                    return defer.promise;
                };
                TableDataService.prototype.GetGroupCategoryTableData = function () {
                    var defer = this.$q.defer();
                    var url = this.api + "GroupCategoryTableData.json";
                    this.$http.get(url)
                        .success(function (result) {
                        defer.resolve(result);
                    })
                        .error(defer.reject);
                    return defer.promise;
                };
                return TableDataService;
            }());
            TableDataService.$inject = ['$http', '$log', '$q', '$window'];
            Services.TableDataService = TableDataService;
            angular.module('NineYi.Sample.Services')
                .service('TableDataService', TableDataService);
        })(Services = Sample.Services || (Sample.Services = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=tableData.services.js.map
var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var UtilityValidationController = (function () {
                function UtilityValidationController(RegexPattern) {
                    this.RegexPattern = RegexPattern;
                }
                return UtilityValidationController;
            }());
            UtilityValidationController.$inject = ["RegexPattern"];
            Controllers.UtilityValidationController = UtilityValidationController;
            angular.module('NineYi.Sample.Controllers')
                .controller('UtilityValidationController', UtilityValidationController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=UtilityValidationController.js.map
var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementToastController = (function () {
                function ElementToastController(NotificationUtility, WindowUtility) {
                    this.NotificationUtility = NotificationUtility;
                    this.WindowUtility = WindowUtility;
                }
                ElementToastController.prototype.showNotificationOnCenter = function () {
                    this.NotificationUtility.ShowNotify("showNotificationOnCenter", this.NotificationUtility.NotificationType.Info, 2000, this.NotificationUtility.positionEnum.Center);
                };
                ElementToastController.prototype.showNotificationOnBottomRight = function () {
                    this.NotificationUtility.ShowNotify("showNotificationOnBottomRight", this.NotificationUtility.NotificationType.Success, 2000, this.NotificationUtility.positionEnum.BottomRight);
                };
                ElementToastController.prototype.showNotificationOnTopRight = function () {
                    this.NotificationUtility.ShowNotify("showNotificationOnTopRight", this.NotificationUtility.NotificationType.Error, 2000, this.NotificationUtility.positionEnum.TopRight);
                };
                ElementToastController.prototype.showCustomNotificationOnCenter = function () {
                    var info = { message: '修改完成' };
                    this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000, this.NotificationUtility.positionEnum.Center);
                };
                ElementToastController.prototype.showCustomNotificationOnCenterReload = function () {
                    var info = { message: '刪除完成' };
                    this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000, this.NotificationUtility.positionEnum.Center, this.NotificationUtility.afterNotificationActionEnum.Reload);
                };
                ElementToastController.prototype.openNotificationSampleWindow = function () {
                    this.WindowUtility.Open("/sample_uxdoc_mapping/example/NotificationSample.html", "Notification 元件");
                };
                ElementToastController.prototype.showCustomNotificationOnCenterCloseWindow = function () {
                    var info = { message: '修改完成' };
                    this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000, this.NotificationUtility.positionEnum.Center, this.NotificationUtility.afterNotificationActionEnum.CloseWindow);
                };
                ElementToastController.prototype.showCustomNotificationOnCenterRedirect = function () {
                    var info = { message: '導回到首頁' };
                    this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000, this.NotificationUtility.positionEnum.Center, this.NotificationUtility.afterNotificationActionEnum.Redirect, '/');
                };
                return ElementToastController;
            }());
            ElementToastController.$inject = ["NotificationUtility", "WindowUtility"];
            Controllers.ElementToastController = ElementToastController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementToastController', ElementToastController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementToastController.js.map
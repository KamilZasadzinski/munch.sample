module NineYi.Sample.Controllers {
    export class UtilityValidationController {
        static $inject = ["RegexPattern"];
        constructor(private RegexPattern:munch.RegexEntity.IRegexConfigEntity){

        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('UtilityValidationController', UtilityValidationController);
}
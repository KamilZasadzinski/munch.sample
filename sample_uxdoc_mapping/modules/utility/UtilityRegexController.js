var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var UtilityRegexController = (function () {
                function UtilityRegexController(RegexPattern) {
                    this.RegexPattern = RegexPattern;
                }
                return UtilityRegexController;
            }());
            UtilityRegexController.$inject = ["RegexPattern"];
            Controllers.UtilityRegexController = UtilityRegexController;
            angular.module('NineYi.Sample.Controllers')
                .controller('UtilityRegexController', UtilityRegexController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=UtilityRegexController.js.map
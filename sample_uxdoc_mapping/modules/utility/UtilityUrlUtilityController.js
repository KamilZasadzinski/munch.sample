var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var UtilityUrlUtilityController = (function () {
                function UtilityUrlUtilityController(UrlUtility) {
                    this.UrlUtility = UrlUtility;
                    console.log(this.UrlUtility.GetQuerystring("Id"));
                    console.log(this.UrlUtility.AppendParameter('/?Id=123', 'test', '234', true));
                }
                return UtilityUrlUtilityController;
            }());
            UtilityUrlUtilityController.$inject = ["UrlUtility"];
            Controllers.UtilityUrlUtilityController = UtilityUrlUtilityController;
            angular.module('NineYi.Sample.Controllers')
                .controller('UtilityUrlUtilityController', UtilityUrlUtilityController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=UtilityUrlUtilityController.js.map
module NineYi.Sample.Controllers {
    export class UtilityRegexController {
        static $inject = ["RegexPattern"];
        constructor(private RegexPattern:munch.RegexEntity.IRegexConfigEntity){

        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('UtilityRegexController', UtilityRegexController);
}
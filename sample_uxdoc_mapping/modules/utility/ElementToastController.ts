module NineYi.Sample.Controllers {
    export class ElementToastController {
        static $inject = ["NotificationUtility","WindowUtility"];
        constructor(private NotificationUtility:munch.NotificationEntity.INotificationEntity,
                    private WindowUtility:munch.WindowUtilityEntity.IWindowUtility){

        }
        public showNotificationOnCenter():void {
            this.NotificationUtility.ShowNotify("showNotificationOnCenter", this.NotificationUtility.NotificationType.Info, 2000,
                this.NotificationUtility.positionEnum.Center);
        }

        public showNotificationOnBottomRight():void {
            this.NotificationUtility.ShowNotify("showNotificationOnBottomRight", this.NotificationUtility.NotificationType.Success, 2000,
                this.NotificationUtility.positionEnum.BottomRight);
        }

        public showNotificationOnTopRight():void {
            this.NotificationUtility.ShowNotify("showNotificationOnTopRight", this.NotificationUtility.NotificationType.Error, 2000,
                this.NotificationUtility.positionEnum.TopRight);
        }

        public showCustomNotificationOnCenter():void {
            var info:munch.NotificationEntity.INotificationInfo = {message: '修改完成'};
            this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000,
                this.NotificationUtility.positionEnum.Center);
        }

        public showCustomNotificationOnCenterReload():void {
            var info:munch.NotificationEntity.INotificationInfo = {message: '刪除完成'};
            this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000,
                this.NotificationUtility.positionEnum.Center,
                this.NotificationUtility.afterNotificationActionEnum.Reload);
        }

        // 要先開視窗才能用js關
        public openNotificationSampleWindow():void{
            this.WindowUtility.Open("/sample_uxdoc_mapping/example/NotificationSample.html", "Notification 元件");
        }
        public showCustomNotificationOnCenterCloseWindow():void {
            var info:munch.NotificationEntity.INotificationInfo = {message: '修改完成'};
            this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000,
                this.NotificationUtility.positionEnum.Center,
                this.NotificationUtility.afterNotificationActionEnum.CloseWindow);
        }

        public showCustomNotificationOnCenterRedirect():void {
            var info:munch.NotificationEntity.INotificationInfo = {message: '導回到首頁'};
            this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000,
                this.NotificationUtility.positionEnum.Center,
                this.NotificationUtility.afterNotificationActionEnum.Redirect,
                '/');
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementToastController', ElementToastController);
}
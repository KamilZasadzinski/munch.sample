module NineYi.Sample.Controllers {
    export class UtilityUrlUtilityController {
        static $inject = ["UrlUtility"];
        constructor(private UrlUtility:munch.UrlUtilityEntity.IUrlUtility){
            console.log(this.UrlUtility.GetQuerystring("Id"));
            console.log(this.UrlUtility.AppendParameter('/?Id=123', 'test', '234',true));

        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('UtilityUrlUtilityController', UtilityUrlUtilityController);
}
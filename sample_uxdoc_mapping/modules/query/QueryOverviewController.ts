module NineYi.Sample.Controllers {
    export class QueryOverviewController {
        static $inject = [];
        /// 時間區間
        public DateTimeTypeList:any[] = [
            { Key:"SalePage_SellingStartDateTime",Value: "銷售開始時間"},
            { Key:"SalePage_SellingEndDateTime",Value: "銷售結束時間"},
            { Key:"SalePage_CreatedDateTime",Value: "依建檔時間"},
            { Key:"SalePage_UpdatedDateTime",Value: "依更新時間"}
        ];

        public L1CategoryList:any[]=[
            {'Name':'分類一', 'Id':'123'},
            {'Name':'分類二', 'Id':'123'},
            {'Name':'分類三', 'Id':'123'},
            {'Name':'分類四', 'Id':'123'}];
        public L2CategoryList:any[]=[
            {'Name':'次分類一', 'Id':'123'},
            {'Name':'分類二', 'Id':'123'},
            {'Name':'分類三', 'Id':'123'},
            {'Name':'分類四', 'Id':'123'}];

        public showAdvanced:boolean = false;
        constructor(){

        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('QueryOverviewController', QueryOverviewController);
}
var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var QueryOverviewController = (function () {
                function QueryOverviewController() {
                    this.DateTimeTypeList = [
                        { Key: "SalePage_SellingStartDateTime", Value: "銷售開始時間" },
                        { Key: "SalePage_SellingEndDateTime", Value: "銷售結束時間" },
                        { Key: "SalePage_CreatedDateTime", Value: "依建檔時間" },
                        { Key: "SalePage_UpdatedDateTime", Value: "依更新時間" }
                    ];
                    this.L1CategoryList = [
                        { 'Name': '分類一', 'Id': '123' },
                        { 'Name': '分類二', 'Id': '123' },
                        { 'Name': '分類三', 'Id': '123' },
                        { 'Name': '分類四', 'Id': '123' }
                    ];
                    this.L2CategoryList = [
                        { 'Name': '次分類一', 'Id': '123' },
                        { 'Name': '分類二', 'Id': '123' },
                        { 'Name': '分類三', 'Id': '123' },
                        { 'Name': '分類四', 'Id': '123' }
                    ];
                    this.showAdvanced = false;
                }
                return QueryOverviewController;
            }());
            QueryOverviewController.$inject = [];
            Controllers.QueryOverviewController = QueryOverviewController;
            angular.module('NineYi.Sample.Controllers')
                .controller('QueryOverviewController', QueryOverviewController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=QueryOverviewController.js.map
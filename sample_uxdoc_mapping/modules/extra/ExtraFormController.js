var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ExtraFormController = (function () {
                function ExtraFormController(RegexPattern) {
                    this.RegexPattern = RegexPattern;
                }
                ExtraFormController.prototype.Submit = function (form) {
                    if (!form.$valid) {
                        return;
                    }
                };
                ExtraFormController.$inject = ["RegexPattern"];
                return ExtraFormController;
            }());
            Controllers.ExtraFormController = ExtraFormController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ExtraFormController', ExtraFormController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ExtraFormController.js.map
var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var SampleWorkshopController = (function () {
                function SampleWorkshopController() {
                    this.CategoryList = [
                        { Id: 1, Name: '服飾' },
                        { Id: 2, Name: '3C' },
                        { Id: 3, Name: '彩妝' },
                        { Id: 4, Name: '傢俱' },
                        { Id: 5, Name: '食品' }
                    ];
                }
                return SampleWorkshopController;
            }());
            SampleWorkshopController.$inject = [];
            Controllers.SampleWorkshopController = SampleWorkshopController;
            angular.module('NineYi.Sample.Controllers')
                .controller('SampleWorkshopController', SampleWorkshopController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=SampleWorkshopController.js.map
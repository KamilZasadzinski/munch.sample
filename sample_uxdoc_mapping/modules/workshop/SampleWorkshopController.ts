module NineYi.Sample.Controllers {
    interface ShopInfoEntity{
        ShopName:string;
        ManagementType:string;
        MobilePhone:number;
        Category:string;
    }
    export class SampleWorkshopController {
        static $inject = [

        ];

        public CategoryList:{}[] = [
            {Id: 1, Name: '服飾'},
            {Id: 2, Name: '3C'},
            {Id: 3, Name: '彩妝'},
            {Id: 4, Name: '傢俱'},
            {Id: 5, Name: '食品'}
        ];

        public ShopInfo:ShopInfoEntity;

        constructor(

        ){
        }
       
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('SampleWorkshopController', SampleWorkshopController);
}
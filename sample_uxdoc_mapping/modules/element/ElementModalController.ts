module NineYi.Sample.Controllers {
    export class ElementModalController {
        static $inject = ['$scope','ModalUtility','NotificationUtility'];
        constructor(
            private $scope:ng.IScope,
            private ModalUtility:munch.ModalEntity.IModalUtilityEntity,
            private NotificationUtility:munch.NotificationEntity.INotificationEntity){

        }

        public OpenModal():void {
            this.ModalUtility.Open("新增", "modules/element/template.modal.html", 'ComponentSampleController', this.$scope, this.ModalUtility.ModalSizeOption.Medium);
        }

        public OpenDeleteModal():void {
            this.ModalUtility.Open("", "modules/element/template.modal.delete.html",
                "ComponentSampleController", this.$scope, this.ModalUtility.ModalSizeOption.Small, "form-modal delete-modal");
        }

        public Delete():void {
            this.ModalUtility.Close();
            var info:munch.NotificationEntity.INotificationInfo = {message: '刪除成功'};
            this.NotificationUtility.ShowNotify(
                info,
                this.NotificationUtility.NotificationType.Custom,
                2000,
                this.NotificationUtility.positionEnum.Center);
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementModalController', ElementModalController);
}
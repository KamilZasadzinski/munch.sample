var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementModalController = (function () {
                function ElementModalController($scope, ModalUtility, NotificationUtility) {
                    this.$scope = $scope;
                    this.ModalUtility = ModalUtility;
                    this.NotificationUtility = NotificationUtility;
                }
                ElementModalController.prototype.OpenModal = function () {
                    this.ModalUtility.Open("新增", "modules/element/template.modal.html", 'ComponentSampleController', this.$scope, this.ModalUtility.ModalSizeOption.Medium);
                };
                ElementModalController.prototype.OpenDeleteModal = function () {
                    this.ModalUtility.Open("", "modules/element/template.modal.delete.html", "ComponentSampleController", this.$scope, this.ModalUtility.ModalSizeOption.Small, "form-modal delete-modal");
                };
                ElementModalController.prototype.Delete = function () {
                    this.ModalUtility.Close();
                    var info = { message: '刪除成功' };
                    this.NotificationUtility.ShowNotify(info, this.NotificationUtility.NotificationType.Custom, 2000, this.NotificationUtility.positionEnum.Center);
                };
                return ElementModalController;
            }());
            ElementModalController.$inject = ['$scope', 'ModalUtility', 'NotificationUtility'];
            Controllers.ElementModalController = ElementModalController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementModalController', ElementModalController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementModalController.js.map
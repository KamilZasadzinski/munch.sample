module NineYi.Sample.Controllers {
    export class ElementDropdownController {
        static $inject = [];

        public City:number;
        public Area:number;
        public TeamMemberOptions:any[];
        private SelectedTeam:number;
        private SelectedName:string;

        constructor(){
            this.TeamMemberOptions =  [
                    {ID: 1, Name: 'Team-A-Member1', TeamID: 1},
                    {ID: 2, Name: 'Team-A-Member2', TeamID: 1},
                    {ID: 3, Name: 'Team-A-Member3', TeamID: 1},
                    {ID: 4, Name: 'Team-B-Member1', TeamID: 2},
                    {ID: 5, Name: 'Team-B-Member2', TeamID: 2},
                    {ID: 6, Name: 'Team-C-Member1', TeamID: 3}
                ];

            //this.SelectedTeam = 1;

        }

        public GetTeamName():void {
            this.SelectedName = this.TeamMemberOptions.filter((value)=>{
                return value.ID === this.SelectedTeam
            })[0]['Name'];
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementDropdownController', ElementDropdownController);
}
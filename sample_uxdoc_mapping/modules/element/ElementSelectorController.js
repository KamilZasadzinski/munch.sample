var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementSelectorController = (function () {
                function ElementSelectorController() {
                    this.SelectedUsers = [{
                            userId: 1,
                            username: "Noctis"
                        }];
                    this.AllUsers = [
                        {
                            userId: 1,
                            username: "Noctis"
                        },
                        {
                            userId: 2,
                            username: "Chris"
                        },
                        {
                            userId: 3,
                            username: "Timothy"
                        },
                        {
                            userId: 4,
                            username: "Kirk"
                        }
                    ];
                }
                return ElementSelectorController;
            }());
            ElementSelectorController.$inject = [];
            Controllers.ElementSelectorController = ElementSelectorController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementSelectorController', ElementSelectorController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementSelectorController.js.map
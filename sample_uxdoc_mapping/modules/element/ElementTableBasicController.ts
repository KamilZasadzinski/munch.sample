module NineYi.Sample.Controllers {
    import IAugmentedJQuery = angular.IAugmentedJQuery;
    export class ElementTableBasicController {
        static $inject = ["$scope", "DialogUtility", "TableDataService"];
        // data
        // public SimpleTableData:any;
        // 本頁全選是否勾選
        public SelectAllStatus:boolean = false;

        ////表格的設定值
        public GetTableData:Function;
        public GridData:any;
        public GridDisplayData:any;
        public TableState:any;

        //// 分頁設定值
        public PageSize:number = 10;
        public PageIndex:number = 1;
        public TotalCount:number = 100;
        
        constructor(
            private $scope:ng.IScope,
            private DialogUtility:munch.DialogEntity.IDialogUtilityEntity,
            private TableDataService: NineYi.Sample.Services.TableDataService){
            this.GridData = [];
            this.GridDisplayData = [];
            // this.TotalCount = 0;
            // this.PageSize = 20;
            // this.PageIndex = 0;
            this.GetTableData = this.GetTable.bind(this);


            this.TableDataService.GetSimpleTableData()
                .then((result:any)=>{
                    this.GridData = result;
                    this.GridData.map((Item)=>{
                        Item.IsChecked = this.SelectAllStatus;
                    });
                    this.GridDisplayData = [].concat(this.GridData);
                });

        }

        public UpdateTable():void{
            this.TableDataService.GetSimpleTableData()
                .then((result:any)=>{
                    this.GridData = result;
                    this.GridData.map((Item)=>{
                        Item.IsChecked = this.SelectAllStatus;
                    });
                    // this.GridDisplayData = [].concat(this.GridData);
                });
        }

        ////st-table
        public GetTable(tableState:any):void {
            this.TableState = tableState;
            this.PageIndex = this.TableState.pagination.start || 0;
            this.PageSize = this.TableState.pagination.number || this.PageSize;
            this.UpdateTable();
        }

        public CheckSelectAll():void{
            let message:string, ids:number[]=[];
            if(this.SelectAllStatus){
                message = "全選有勾";
            }else{
                message = "全選沒勾";
            }
            this.GridData.map((Item)=>{
                if(Item.IsChecked){
                    ids.push(Item.Id);
                }
            });
            if(ids.length>0){
                message += "，所選Item Id: " + ids.join(", ");

            }
            this.DialogUtility.Notice(message);
        }

        /// 下面選項的item
        public CheckedItem(index:number):void{
            this.SelectAllStatus = false;
        }

        //// 設定 本頁全選 勾選與否 在本頁的動作
        public SelectAllSwitch():void{
            this.$scope.$watch('SelectAllStatus', ()=>{
                this.GridData.map((Item)=>{
                    Item.IsChecked = this.SelectAllStatus;
                });
            });
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementTableBasicController', ElementTableBasicController);
}

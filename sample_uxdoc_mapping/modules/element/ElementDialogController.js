var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementDialogController = (function () {
                function ElementDialogController(DialogUtility, $translate) {
                    this.DialogUtility = DialogUtility;
                    this.$translate = $translate;
                    this.longMsg = "「我的商品有常溫也有冷凍，運費不同，該怎麼收費？」、「消費者下單後，宅配卻無人收件，怎麼辦？」、「商品有貴有便宜，信用卡分期該如何設定？」為了因應店家的更多元的業務需求。91APP系統金物流重大功能開發，已大功告成囉！今天小編鄭重介紹金物流的三大新增功能，讓各位店家大大們在經營上，能有更好的發揮。";
                }
                ElementDialogController.prototype.OpenDialogNotice = function () {
                    this.DialogUtility.Notice("基本 Dialog，沒有標題和關閉按鈕");
                };
                ElementDialogController.prototype.OpenDialogNoticeWithTitle = function () {
                    this.DialogUtility.Notice("只有標題 Dialog", "標題文字");
                };
                ElementDialogController.prototype.OpenDialogNoticeWithClose = function () {
                    this.DialogUtility.Notice("只有關閉按鈕 Dialog", null, true);
                };
                ElementDialogController.prototype.OpenDialogNoticeWithtitleClose = function () {
                    this.DialogUtility.Notice("有標題和關閉按鈕 Dialog", "標題文字", true);
                };
                ElementDialogController.prototype.OpenDialogAutoCloseNotice = function () {
                    this.DialogUtility.AutoCloseNotice("自動關閉的Dialog");
                };
                ElementDialogController.prototype.OpenDialogConfirm = function () {
                    this.DialogUtility.Confirm("基本無標題和關閉按鈕 dialog confirm");
                };
                ElementDialogController.prototype.OpenDialogConfirmWithTitle = function () {
                    this.DialogUtility.Confirm("只有標題 dialog confirm", "", "", false, true, true, "標題文字");
                };
                ElementDialogController.prototype.OpenDialogConfirmWithClose = function () {
                    this.DialogUtility.Confirm("只有關閉按鈕 dialog confirm", "", "", false, true, true, null, true);
                };
                ElementDialogController.prototype.OpenDialogConfirmWithTitleClose = function () {
                    this.DialogUtility.Confirm("有標題和關閉按鈕 dialog confirm", "", "", false, true, true, "標題文字", true);
                };
                ElementDialogController.prototype.OpenDialogConfirmLongMsg = function () {
                    this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false);
                };
                ElementDialogController.prototype.OpenDialogConfirmLongMsgWithTitle = function () {
                    this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false, "標題文字");
                };
                ElementDialogController.prototype.OpenDialogConfirmLongMsgWithClose = function () {
                    this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false, null, true);
                };
                ElementDialogController.prototype.OpenDialogConfirmLongMsgWithTitleClose = function () {
                    this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false, "標題文字", true);
                };
                ElementDialogController.prototype.OpenDialogDelete = function () {
                    this.DialogUtility.Delete("是否刪除？");
                };
                ElementDialogController.prototype.OpenDialogDeleteWithTitle = function () {
                    this.DialogUtility.Delete("是否刪除？", "", false, "標題文字");
                };
                ElementDialogController.prototype.OpenDialogNoticeFromServer = function () {
                    this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>");
                };
                ElementDialogController.prototype.OpenDialogNoticeFromServerWithTitle = function () {
                    this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>", true, "標題文字");
                };
                ElementDialogController.prototype.OpenDialogNoticeFromServerWithClose = function () {
                    this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>", true, null, true);
                };
                ElementDialogController.prototype.OpenDialogNoticeFromServerWithTitleClose = function () {
                    this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>", true, "標題文字", true);
                };
                ElementDialogController.prototype.OpenDialogRedirect = function () {
                    this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁");
                };
                ElementDialogController.prototype.OpenDialogRedirectWithTitle = function () {
                    this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁", "標題文字");
                };
                ElementDialogController.prototype.OpenDialogRedirectWithClose = function () {
                    this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁", null, true);
                };
                ElementDialogController.prototype.OpenDialogRedirectWithTitleClose = function () {
                    this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁", "標題文字", true);
                };
                ElementDialogController.prototype.OpenDialogReloadRedirect = function () {
                    this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁");
                };
                ElementDialogController.prototype.OpenDialogReloadRedirectWithTitle = function () {
                    this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁", "標題文字");
                };
                ElementDialogController.prototype.OpenDialogReloadRedirectWithClose = function () {
                    this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁", null, true);
                };
                ElementDialogController.prototype.OpenDialogReloadRedirectWithTitleClose = function () {
                    this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁", "標題文字", true);
                };
                return ElementDialogController;
            }());
            ElementDialogController.$inject = ["DialogUtility", "$translate"];
            Controllers.ElementDialogController = ElementDialogController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementDialogController', ElementDialogController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementDialogController.js.map
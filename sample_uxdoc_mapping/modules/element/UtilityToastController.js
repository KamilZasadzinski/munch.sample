var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var UtilityToastController = (function () {
                function UtilityToastController(ToastUtility, DialogUtility) {
                    this.ToastUtility = ToastUtility;
                    this.DialogUtility = DialogUtility;
                }
                UtilityToastController.prototype.ShowToast = function () {
                    this.ToastUtility.Show("刪除完成");
                };
                UtilityToastController.prototype.ShowLongToast = function () {
                    this.ToastUtility.Custom({ message: "比較長的訊息，多行會自動展開，繼續很多字的訊息...", duration: 5000 });
                };
                UtilityToastController.prototype.ShowDialog = function () {
                    var _this = this;
                    this.ToastUtility.Show("動作一完成")
                        .then(function () {
                        _this.DialogUtility.Notice("繼續進行動作二");
                    });
                };
                return UtilityToastController;
            }());
            UtilityToastController.$inject = ["ToastUtility", "DialogUtility"];
            Controllers.UtilityToastController = UtilityToastController;
            angular.module('NineYi.Sample.Controllers')
                .controller('UtilityToastController', UtilityToastController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=UtilityToastController.js.map
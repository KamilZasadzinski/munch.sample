module NineYi.Sample.Controllers {
    export class UtilityToastController {
        static $inject = ["ToastUtility", "DialogUtility"];
        constructor(private ToastUtility:munch.ToastEntity.IToastUtilityEntity,
                    private DialogUtility: munch.DialogEntity.IDialogUtilityEntity){

        }

        public ShowToast(){
            this.ToastUtility.Show("刪除完成");
        }
        public ShowLongToast(){
            this.ToastUtility.Custom({message: "比較長的訊息，多行會自動展開，繼續很多字的訊息...", duration: 5000});
        }
        public ShowDialog(){
            this.ToastUtility.Show("動作一完成")
                .then(()=>{
                    this.DialogUtility.Notice("繼續進行動作二");
                });
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('UtilityToastController', UtilityToastController);
}
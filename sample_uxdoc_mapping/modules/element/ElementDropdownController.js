var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementDropdownController = (function () {
                function ElementDropdownController() {
                    this.TeamMemberOptions = [
                        { ID: 1, Name: 'Team-A-Member1', TeamID: 1 },
                        { ID: 2, Name: 'Team-A-Member2', TeamID: 1 },
                        { ID: 3, Name: 'Team-A-Member3', TeamID: 1 },
                        { ID: 4, Name: 'Team-B-Member1', TeamID: 2 },
                        { ID: 5, Name: 'Team-B-Member2', TeamID: 2 },
                        { ID: 6, Name: 'Team-C-Member1', TeamID: 3 }
                    ];
                }
                ElementDropdownController.prototype.GetTeamName = function () {
                    var _this = this;
                    this.SelectedName = this.TeamMemberOptions.filter(function (value) {
                        return value.ID === _this.SelectedTeam;
                    })[0]['Name'];
                };
                return ElementDropdownController;
            }());
            ElementDropdownController.$inject = [];
            Controllers.ElementDropdownController = ElementDropdownController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementDropdownController', ElementDropdownController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementDropdownController.js.map
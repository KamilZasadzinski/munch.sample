module NineYi.Sample.Controllers {
    import IAugmentedJQuery = angular.IAugmentedJQuery;
    export class ElementTableCollapseController {
        static $inject = ["$scope", "DialogUtility", "TableDataService"];
        public GroupedOrderTableData:any;

        //// 分頁設定值
        public PageSize:number = 10;
        public PageIndex:number = 1;
        public TotalCount:number = 100;
        public SelectedPageSize:number = 10;
        
        constructor(
            private $scope:ng.IScope,
            private DialogUtility:munch.DialogEntity.IDialogUtilityEntity,
            private TableDataService: NineYi.Sample.Services.TableDataService){

            this.TableDataService.GetGroupedOrderTableData()
                .then((result:any)=>{
                    this.GroupedOrderTableData = result;
                });
        }


        //// 是否顯示出貨按鈕(各ＴＳ單)
        public ShowShippingBtn(index:number):boolean {
            return  index % 2 === 1 || index % 4 === 0;
        }

        //// 是否顯示驗退按鈕(各ＴＳ單)
        public ShowVerifyReturnBtn(index:number):boolean {
            return  index % 2 === 0;
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementTableCollapseController', ElementTableCollapseController);
}

var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementTableCategoryController = (function () {
                function ElementTableCategoryController($scope, DialogUtility, TableDataService) {
                    var _this = this;
                    this.$scope = $scope;
                    this.DialogUtility = DialogUtility;
                    this.TableDataService = TableDataService;
                    this.PageSize = 10;
                    this.PageIndex = 1;
                    this.TotalCount = 100;
                    this.SelectedPageSize = 10;
                    this.TableDataService.GetGroupCategoryTableData()
                        .then(function (result) {
                        _this.TableCategoryData = result;
                    });
                }
                return ElementTableCategoryController;
            }());
            ElementTableCategoryController.$inject = ["$scope", "DialogUtility", "TableDataService"];
            Controllers.ElementTableCategoryController = ElementTableCategoryController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementTableCategoryController', ElementTableCategoryController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementTableCategoryController.js.map
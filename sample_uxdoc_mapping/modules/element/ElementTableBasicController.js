var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementTableBasicController = (function () {
                function ElementTableBasicController($scope, DialogUtility, TableDataService) {
                    var _this = this;
                    this.$scope = $scope;
                    this.DialogUtility = DialogUtility;
                    this.TableDataService = TableDataService;
                    this.SelectAllStatus = false;
                    this.PageSize = 10;
                    this.PageIndex = 1;
                    this.TotalCount = 100;
                    this.GridData = [];
                    this.GridDisplayData = [];
                    this.GetTableData = this.GetTable.bind(this);
                    this.TableDataService.GetSimpleTableData()
                        .then(function (result) {
                        _this.GridData = result;
                        _this.GridData.map(function (Item) {
                            Item.IsChecked = _this.SelectAllStatus;
                        });
                        _this.GridDisplayData = [].concat(_this.GridData);
                    });
                }
                ElementTableBasicController.prototype.UpdateTable = function () {
                    var _this = this;
                    this.TableDataService.GetSimpleTableData()
                        .then(function (result) {
                        _this.GridData = result;
                        _this.GridData.map(function (Item) {
                            Item.IsChecked = _this.SelectAllStatus;
                        });
                    });
                };
                ElementTableBasicController.prototype.GetTable = function (tableState) {
                    this.TableState = tableState;
                    this.PageIndex = this.TableState.pagination.start || 0;
                    this.PageSize = this.TableState.pagination.number || this.PageSize;
                    this.UpdateTable();
                };
                ElementTableBasicController.prototype.CheckSelectAll = function () {
                    var message, ids = [];
                    if (this.SelectAllStatus) {
                        message = "全選有勾";
                    }
                    else {
                        message = "全選沒勾";
                    }
                    this.GridData.map(function (Item) {
                        if (Item.IsChecked) {
                            ids.push(Item.Id);
                        }
                    });
                    if (ids.length > 0) {
                        message += "，所選Item Id: " + ids.join(", ");
                    }
                    this.DialogUtility.Notice(message);
                };
                ElementTableBasicController.prototype.CheckedItem = function (index) {
                    this.SelectAllStatus = false;
                };
                ElementTableBasicController.prototype.SelectAllSwitch = function () {
                    var _this = this;
                    this.$scope.$watch('SelectAllStatus', function () {
                        _this.GridData.map(function (Item) {
                            Item.IsChecked = _this.SelectAllStatus;
                        });
                    });
                };
                return ElementTableBasicController;
            }());
            ElementTableBasicController.$inject = ["$scope", "DialogUtility", "TableDataService"];
            Controllers.ElementTableBasicController = ElementTableBasicController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementTableBasicController', ElementTableBasicController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementTableBasicController.js.map
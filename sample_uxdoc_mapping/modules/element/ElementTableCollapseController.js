var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var ElementTableCollapseController = (function () {
                function ElementTableCollapseController($scope, DialogUtility, TableDataService) {
                    var _this = this;
                    this.$scope = $scope;
                    this.DialogUtility = DialogUtility;
                    this.TableDataService = TableDataService;
                    this.PageSize = 10;
                    this.PageIndex = 1;
                    this.TotalCount = 100;
                    this.SelectedPageSize = 10;
                    this.TableDataService.GetGroupedOrderTableData()
                        .then(function (result) {
                        _this.GroupedOrderTableData = result;
                    });
                }
                ElementTableCollapseController.prototype.ShowShippingBtn = function (index) {
                    return index % 2 === 1 || index % 4 === 0;
                };
                ElementTableCollapseController.prototype.ShowVerifyReturnBtn = function (index) {
                    return index % 2 === 0;
                };
                return ElementTableCollapseController;
            }());
            ElementTableCollapseController.$inject = ["$scope", "DialogUtility", "TableDataService"];
            Controllers.ElementTableCollapseController = ElementTableCollapseController;
            angular.module('NineYi.Sample.Controllers')
                .controller('ElementTableCollapseController', ElementTableCollapseController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=ElementTableCollapseController.js.map
module NineYi.Sample.Controllers {
    export class ElementSelectorController {
        static $inject = [];

        public AllUsers:any[];
        public SelectedUsers:any[];

        constructor(){
            this.SelectedUsers = [{
                userId: 1,
                username: "Noctis"
            }];

            this.AllUsers = [
                {
                    userId: 1,
                    username: "Noctis"
                },
                {
                    userId: 2,
                    username: "Chris"
                },
                {
                    userId: 3,
                    username: "Timothy"
                },
                {
                    userId: 4,
                    username: "Kirk"
                }];
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementSelectorController', ElementSelectorController);
}
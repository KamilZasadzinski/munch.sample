module NineYi.Sample.Controllers {
    import IAugmentedJQuery = angular.IAugmentedJQuery;
    export class ElementTableCategoryController {
        static $inject = ["$scope", "DialogUtility", "TableDataService"];
        public TableCategoryData:any;

        //// 分頁設定值
        public PageSize:number = 10;
        public PageIndex:number = 1;
        public TotalCount:number = 100;
        public SelectedPageSize:number = 10;
        
        constructor(
            private $scope:ng.IScope,
            private DialogUtility:munch.DialogEntity.IDialogUtilityEntity,
            private TableDataService: NineYi.Sample.Services.TableDataService){

            this.TableDataService.GetGroupCategoryTableData()
                .then((result:any)=>{
                    this.TableCategoryData = result;
                });
        }

    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementTableCategoryController', ElementTableCategoryController);
}

module NineYi.Sample.Controllers {
    export class ElementDialogController {
        static $inject = ["DialogUtility","$translate"];
        private longMsg:string;
        constructor(
            private DialogUtility:munch.DialogEntity.IDialogUtilityEntity,
            private $translate:ng.translate.ITranslateService
        ){
            this.longMsg = "「我的商品有常溫也有冷凍，運費不同，該怎麼收費？」、「消費者下單後，宅配卻無人收件，怎麼辦？」、「商品有貴有便宜，信用卡分期該如何設定？」為了因應店家的更多元的業務需求。91APP系統金物流重大功能開發，已大功告成囉！今天小編鄭重介紹金物流的三大新增功能，讓各位店家大大們在經營上，能有更好的發揮。";
        }
        public OpenDialogNotice():void {
            this.DialogUtility.Notice("基本 Dialog，沒有標題和關閉按鈕");
        }
        public OpenDialogNoticeWithTitle():void {
            this.DialogUtility.Notice("只有標題 Dialog", "標題文字");
        }
        public OpenDialogNoticeWithClose():void {
            this.DialogUtility.Notice("只有關閉按鈕 Dialog", null, true);
        }
        public OpenDialogNoticeWithtitleClose():void {
            this.DialogUtility.Notice("有標題和關閉按鈕 Dialog", "標題文字", true);
        }
        public OpenDialogAutoCloseNotice():void {
            this.DialogUtility.AutoCloseNotice("自動關閉的Dialog");
        }

        public OpenDialogConfirm():void {
            this.DialogUtility.Confirm("基本無標題和關閉按鈕 dialog confirm");
        }
        public OpenDialogConfirmWithTitle():void {
            this.DialogUtility.Confirm("只有標題 dialog confirm", "", "", false, true, true, "標題文字");
        }
        public OpenDialogConfirmWithClose():void {
            this.DialogUtility.Confirm("只有關閉按鈕 dialog confirm", "", "", false, true, true, null, true);
        }
        public OpenDialogConfirmWithTitleClose():void {
            this.DialogUtility.Confirm("有標題和關閉按鈕 dialog confirm", "", "", false, true, true, "標題文字", true);
        }

        public OpenDialogConfirmLongMsg():void {
            this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false);
        }
        public OpenDialogConfirmLongMsgWithTitle():void {
            this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false, "標題文字");
        }
        public OpenDialogConfirmLongMsgWithClose():void {
            this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false, null, true);
        }
        public OpenDialogConfirmLongMsgWithTitleClose():void {
            this.DialogUtility.Confirm(this.longMsg, "", "", false, true, false, "標題文字", true);
        }

        public OpenDialogDelete():void {
            this.DialogUtility.Delete("是否刪除？");
        }
        public OpenDialogDeleteWithTitle():void {
            this.DialogUtility.Delete("是否刪除？", "", false, "標題文字");
        }

        public OpenDialogNoticeFromServer():void {
            this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>");
        }

        public OpenDialogNoticeFromServerWithTitle():void {
            this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>", true, "標題文字");
        }

        public OpenDialogNoticeFromServerWithClose():void {
            this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>", true, null, true);
        }

        public OpenDialogNoticeFromServerWithTitleClose():void {
            this.DialogUtility.NoticeFromServer("從server端傳訊息進來 \n <b>html 會直接輸出</b>", true, "標題文字", true);
        }

        public OpenDialogRedirect():void {
            this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁");
        }

        public OpenDialogRedirectWithTitle():void {
            this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁", "標題文字");
        }

        public OpenDialogRedirectWithClose():void {
            this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁", null, true);
        }

        public OpenDialogRedirectWithTitleClose():void {
            this.DialogUtility.Redirect("儲存成功", "回總覽", '/', "停留原頁", "標題文字", true);
        }

        public OpenDialogReloadRedirect():void {
            this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁");
        }
        public OpenDialogReloadRedirectWithTitle():void {
            this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁", "標題文字");
        }
        public OpenDialogReloadRedirectWithClose():void {
            this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁", null, true);
        }
        public OpenDialogReloadRedirectWithTitleClose():void {
            this.DialogUtility.RedirectReload("Reload and Redirect", "回總覽", '/', "停留原頁", "標題文字", true);
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('ElementDialogController', ElementDialogController);
}
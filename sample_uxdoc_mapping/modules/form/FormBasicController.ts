module NineYi.Sample.Controllers {
    export class FormBasicController {
        static $inject = ["RegexPattern"];
        constructor(private RegexPattern:munch.RegexEntity.IRegexConfigEntity){

        }

        Submit(form:ng.IFormController){
            if(!form.$valid){
                return;
            }
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('FormBasicController', FormBasicController);
}
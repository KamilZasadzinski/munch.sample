var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var FormBasicController = (function () {
                function FormBasicController(RegexPattern) {
                    this.RegexPattern = RegexPattern;
                }
                FormBasicController.prototype.Submit = function (form) {
                    if (!form.$valid) {
                        return;
                    }
                };
                return FormBasicController;
            }());
            FormBasicController.$inject = ["RegexPattern"];
            Controllers.FormBasicController = FormBasicController;
            angular.module('NineYi.Sample.Controllers')
                .controller('FormBasicController', FormBasicController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=FormBasicController.js.map
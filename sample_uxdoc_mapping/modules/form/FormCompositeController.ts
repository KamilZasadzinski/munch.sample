module NineYi.Sample.Controllers {
    export class FormCompositeController {
        static $inject = ["RegexPattern"];
        constructor(private RegexPattern:munch.RegexEntity.IRegexConfigEntity){

        }

        Submit(form:ng.IFormController){
            if(!form.$valid){
                return;
            }
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('FormCompositeController', FormCompositeController);
}
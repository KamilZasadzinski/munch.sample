var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var FormCompositeController = (function () {
                function FormCompositeController(RegexPattern) {
                    this.RegexPattern = RegexPattern;
                }
                FormCompositeController.prototype.Submit = function (form) {
                    if (!form.$valid) {
                        return;
                    }
                };
                return FormCompositeController;
            }());
            FormCompositeController.$inject = ["RegexPattern"];
            Controllers.FormCompositeController = FormCompositeController;
            angular.module('NineYi.Sample.Controllers')
                .controller('FormCompositeController', FormCompositeController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=FormCompositeController.js.map
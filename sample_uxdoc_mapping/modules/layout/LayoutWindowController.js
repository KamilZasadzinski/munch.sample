var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var LayoutWindowController = (function () {
                function LayoutWindowController(WindowUtility) {
                    this.WindowUtility = WindowUtility;
                }
                LayoutWindowController.prototype.OpenWindow = function () {
                    this.WindowUtility.Open("/v2/location/create/64", "新增門市會員");
                };
                return LayoutWindowController;
            }());
            LayoutWindowController.$inject = ["WindowUtility"];
            Controllers.LayoutWindowController = LayoutWindowController;
            angular.module('NineYi.Sample.Controllers')
                .controller('LayoutWindowController', LayoutWindowController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=LayoutWindowController.js.map
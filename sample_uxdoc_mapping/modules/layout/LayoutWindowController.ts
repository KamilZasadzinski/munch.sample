module NineYi.Sample.Controllers {
    export class LayoutWindowController {
        static $inject = ["WindowUtility"];
        constructor(private WindowUtility:munch.WindowUtilityEntity.IWindowUtility){

        }
        public OpenWindow():void {
            this.WindowUtility.Open("/v2/location/create/64", "新增門市會員");
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('LayoutWindowController', LayoutWindowController);
}
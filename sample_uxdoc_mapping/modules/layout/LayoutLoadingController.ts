module NineYi.Sample.Controllers {
    export class LayoutLoadingController {
        static $inject = ['LoadingUtility','$timeout'];
        constructor(private LoadingUtility:munch.LoadingEntity.ILoadingUtilityEntity,
                    private $timeout:ng.ITimeoutService
        ){

        }
        public ShowLoading():void {
            this.LoadingUtility.Start();
            this.$timeout(()=> {
                // Stop the block after some async operation.
                this.LoadingUtility.Stop();
            }, 1000);
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('LayoutLoadingController', LayoutLoadingController);
}
var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var LayoutBasicController = (function () {
                function LayoutBasicController() {
                }
                return LayoutBasicController;
            }());
            LayoutBasicController.$inject = [];
            Controllers.LayoutBasicController = LayoutBasicController;
            angular.module('NineYi.Sample.Controllers')
                .controller('LayoutBasicController', LayoutBasicController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=LayoutBasicController.js.map
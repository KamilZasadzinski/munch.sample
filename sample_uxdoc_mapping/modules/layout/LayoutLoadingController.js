var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var LayoutLoadingController = (function () {
                function LayoutLoadingController(LoadingUtility, $timeout) {
                    this.LoadingUtility = LoadingUtility;
                    this.$timeout = $timeout;
                }
                LayoutLoadingController.prototype.ShowLoading = function () {
                    var _this = this;
                    this.LoadingUtility.Start();
                    this.$timeout(function () {
                        _this.LoadingUtility.Stop();
                    }, 1000);
                };
                return LayoutLoadingController;
            }());
            LayoutLoadingController.$inject = ['LoadingUtility', '$timeout'];
            Controllers.LayoutLoadingController = LayoutLoadingController;
            angular.module('NineYi.Sample.Controllers')
                .controller('LayoutLoadingController', LayoutLoadingController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=LayoutLoadingController.js.map
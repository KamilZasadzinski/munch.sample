module NineYi.Sample.Controllers {
    export class Routes {
        static $inject = ['$stateProvider'];
        constructor(private $stateProvider){
            $stateProvider
                .state('index', {
                    url: '/index',
                    templateUrl: '../sample_uxdoc_mapping/modules/index.html'
                })
                .state('quickstart', {
                    abstract: true,
                    url: '/quickstart',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/index.html'
                })
                .state('quickstart.bundle', {
                    url: '/bundle',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/bundle.html'
                })
                .state('quickstart.controller', {
                    url: '/controller',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/controller.html'
                })
                .state('quickstart.icon', {
                    url: '/icon',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/icon.html'
                })
                .state('quickstart.layout', {
                    url: '/layout',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/layout.html'
                })
                .state('quickstart.service', {
                    url: '/service',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/service.html'
                })
                .state('quickstart.translate', {
                    url: '/translate',
                    templateUrl: '../sample_uxdoc_mapping/modules/quickstart/translate.html'
                })
                .state('layout', {
                    abstract: true,
                    url: '/layout',
                    templateUrl: '../sample_uxdoc_mapping/modules/layout/index.html'
                })
                .state('layout.basic', {
                    url: '/basic',
                    templateUrl: '../sample_uxdoc_mapping/modules/layout/basic.html',
                    controller: 'LayoutBasicController',
                    controllerAs: 'LayoutBasicCtrl'
                })
                .state('layout.window', {
                    url: '/window',
                    templateUrl: '../sample_uxdoc_mapping/modules/layout/window.html',
                    controller: 'LayoutWindowController',
                    controllerAs: 'LayoutWindowCtrl'
                })
                .state('layout.popup', {
                    url: '/popup',
                    templateUrl: '../sample_uxdoc_mapping/modules/layout/popup.html'
                })
                .state('layout.loading', {
                    url: '/loading',
                    templateUrl: '../sample_uxdoc_mapping/modules/layout/loading.html',
                    controller: 'LayoutLoadingController',
                    controllerAs: 'LayoutLoadingCtrl'
                })
                .state('query', {
                    abstract: true,
                    url: '/query',
                    templateUrl: '../sample_uxdoc_mapping/modules/query/index.html'
                })
                .state('query.overview', {
                    url: '/overview',
                    templateUrl: '../sample_uxdoc_mapping/modules/query/overview.html',
                    controller: 'QueryOverviewController',
                    controllerAs: 'QueryOverviewCtrl'
                })
                .state('query.condition', {
                    url: '/condition',
                    templateUrl: '../sample_uxdoc_mapping/modules/query/condition.html'
                })
                .state('query.relate', {
                    url: '/relate',
                    templateUrl: '../sample_uxdoc_mapping/modules/query/relate.html'
                })
                .state('query.table', {
                    url: '/table',
                    templateUrl: '../sample_uxdoc_mapping/modules/query/table.html'
                })
                .state('setting', {
                    abstract: true,
                    url: '/setting',
                    templateUrl: '../sample_uxdoc_mapping/modules/setting/index.html'
                })
                .state('setting.overview', {
                    url: '/overview',
                    templateUrl: '../sample_uxdoc_mapping/modules/setting/overview.html'
                })
                .state('setting.window', {
                    url: '/window',
                    templateUrl: '../sample_uxdoc_mapping/modules/setting/window.html'
                })
                .state('setting.popup', {
                    url: '/popup',
                    templateUrl: '../sample_uxdoc_mapping/modules/setting/popup.html'
                })
                .state('setting.actions', {
                    url: '/actions',
                    templateUrl: '../sample_uxdoc_mapping/modules/setting/actions.html'
                })
                .state('setting.actionContainer', {
                    url: '/actionContainer',
                    templateUrl: '../sample_uxdoc_mapping/modules/setting/actionContainer.html'
                })
                .state('element', {
                    abstract: true,
                    url: '/element',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/index.html'
                })
                .state('element.tab', {
                    url: '/tab',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tab.html'
                })
                .state('element.explain', {
                    url: '/explain',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/explain.html'
                })
                .state('element.toast', {
                    url: '/toast',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/toastUtility.html',
                    controller: 'UtilityToastController',
                    controllerAs: 'UtilityToastCtrl'
                })
                .state('element.tooltip', {
                    url: '/tooltip',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tooltip.html'
                })
                .state('element.breadcrumb', {
                    url: '/breadcrumb',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/breadcrumb.html'
                })
                .state('element.symbol', {
                    url: '/symbol',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/symbol.html'
                })
                .state('element.input', {
                    url: '/input',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/input.html'
                })
                .state('element.checkbox', {
                    url: '/checkbox',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/checkbox.html'
                })
                .state('element.radio', {
                    url: '/radio',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/radio.html'
                })
                .state('element.dropdown', {
                    url: '/dropdown',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/dropdown.html',
                    controller: 'ElementDropdownController',
                    controllerAs: 'ElementDropdownCtrl'
                })
                .state('element.editor', {
                    url: '/editor',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/editor.html'
                })
                .state('element.datetime', {
                    url: '/datetime',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/datetime.html'
                })
                .state('element.imageUpload', {
                    url: '/imageUpload',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/imageUpload.html'
                })
                .state('element.fileUpload', {
                    url: '/imageUpload',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/fileUpload.html'
                })
                .state('element.selector', {
                    url: '/selector',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/selector.html',
                    controller: 'ElementSelectorController',
                    controllerAs: 'ElementSelectorCtrl'
                })
                .state('element.other', {
                    url: '/other',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/other.html'
                })
                .state('table', {
                    abstract: true,
                    url: '/table',
                    templateUrl: '../sample_uxdoc_mapping/modules/table/index.html'
                })
                .state('table.tableComponent', {
                    url: '/tableComponent',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tableComponent.html'
                })
                .state('table.tableLayout', {
                    url: '/tableLayout',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tableLayout.html'
                })
                .state('table.tableBasic', {
                    url: '/tableBasic',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tableBasic.html',
                    controller: 'ElementTableBasicController',
                    controllerAs: 'ElementTableBasicCtrl'
                })
                .state('table.tableCheckbox', {
                    url: '/tableCheckbox',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tableCheckbox.html',
                    controller: 'ElementTableBasicController',
                    controllerAs: 'ElementTableBasicCtrl'
                })
                .state('table.tableCollapse', {
                    url: '/tableCollapse',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tableCollapse.html',
                    controller: 'ElementTableCollapseController',
                    controllerAs: 'ElementTableCollapseCtrl'
                })
                .state('table.tableCategory', {
                    url: '/tableCategory',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/tableCategory.html',
                    controller: 'ElementTableCategoryController',
                    controllerAs: 'ElementTableCategoryCtrl'
                })
                .state('form', {
                    abstract: true,
                    url: '/form',
                    templateUrl: '../sample_uxdoc_mapping/modules/form/index.html'
                })
                .state('form.basic', {
                    url: '/basic',
                    templateUrl: '../sample_uxdoc_mapping/modules/form/basic.html',
                    controller: 'FormBasicController',
                    controllerAs: 'FormBasicCtrl'
                })
                .state('form.composite', {
                    url: '/composite',
                    templateUrl: '../sample_uxdoc_mapping/modules/form/composite.html',
                    controller: 'FormCompositeController',
                    controllerAs: 'FormCompositeCtrl'
                })
                .state('form.actionContainer', {
                    url: '/actionContainer',
                    templateUrl: '../sample_uxdoc_mapping/modules/form/actionContainer.html'
                })
                .state('form.css', {
                    url: '/css',
                    templateUrl: '../sample_uxdoc_mapping/modules/form/css.html'
                })
                .state('style', {
                    abstract: true,
                    url: '/style',
                    templateUrl: '../sample_uxdoc_mapping/modules/style/index.html'
                })
                .state('style.css', {
                    url: '/css',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/css.html'
                })
                .state('style.button', {
                    url: '/button',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/button.html'
                })
                .state('style.panel', {
                    url: '/panel',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/panel.html'
                })
                .state('utility', {
                    abstract: true,
                    url: '/utility',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/index.html'
                })
                .state('utility.dialog', {
                    url: '/dialog',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/dialog.html',
                    controller: 'ElementDialogController',
                    controllerAs: 'ElementDialogCtrl'
                })
                .state('utility.modal', {
                    url: '/modal',
                    templateUrl: '../sample_uxdoc_mapping/modules/element/modal.html',
                    controller: 'ElementModalController',
                    controllerAs: 'ElementModalCtrl'
                })
                .state('utility.regex', {
                    url: '/regex',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/regex.html',
                    controller: 'UtilityRegexController',
                    controllerAs: 'UtilityRegexCtrl'
                })
                .state('utility.validation', {
                    url: '/validation',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/validation.html',
                    controller: 'UtilityValidationController',
                    controllerAs: 'UtilityValidationCtrl'
                })
                .state('utility.urlUtility', {
                    url: '/urlUtility',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/urlUtility.html',
                    controller: 'UtilityUrlUtilityController',
                    controllerAs: 'UtilityUrlUtilityCtrl'
                })
                .state('utility.notation', {
                    url: '/notation',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/notation.html'
                })
                .state('utility.notification', {
                    url: '/toastUtility',
                    templateUrl: '../sample_uxdoc_mapping/modules/utility/toast.html',
                    controller: 'ElementToastController',
                    controllerAs: 'ElementToastCtrl'
                })
                .state('workshop', {
                    abstract: true,
                    url: '/workshop',
                    templateUrl: '../sample_uxdoc_mapping/modules/workshop/index.html'
                })
                .state('workshop.july', {
                    url: '/july',
                    templateUrl: '../sample_uxdoc_mapping/modules/workshop/july.html',
                    controller: 'SampleWorkshopController',
                    controllerAs: 'SampleWorkshopCtrl'
                });
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .config(Routes);
}
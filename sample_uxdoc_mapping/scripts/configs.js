var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Configs;
        (function (Configs) {
            angular.module('NineYi.Sample.Configs')
                .config(['$urlRouterProvider',
                function ($urlRouterProvider) {
                    $urlRouterProvider.otherwise('/index');
                }]);
        })(Configs = Sample.Configs || (Sample.Configs = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=configs.js.map
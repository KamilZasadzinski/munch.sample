module NineYi.Sample.Configs {
    angular.module('NineYi.Sample.Configs')
        .config(['$urlRouterProvider',
            function ($urlRouterProvider) {
            // if the path doesn't match any of the urls you configured
            // otherwise will take care of routing the user to the specified url
            $urlRouterProvider.otherwise('/index');
        }]);
}

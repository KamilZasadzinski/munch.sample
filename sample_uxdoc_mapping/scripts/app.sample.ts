module NineYi.Sample {
    angular.module('NineYi.Sample', [
        'ui.router',
        'ng.prism',
        'NineYi.FrontEnd.Munch',
        'NineYi.Sample.Controllers',
        'NineYi.Sample.Configs']);

    angular.module('NineYi.Sample.Controllers', ['NineYi.Sample.Services']);
    angular.module('NineYi.Sample.Configs', []);
    angular.module('NineYi.Sample.Services', []);
}

module NineYi.Sample.Controllers {
    export class MenuController {
        static $inject = [];
        constructor(){}
        public IsActive(state):boolean{
            return window.location.hash.indexOf(state) > 0;
        }
    }
    angular.module('NineYi.Sample.Controllers')
        .controller('MenuController', MenuController);
}
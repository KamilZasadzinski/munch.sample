angular.module('ng.prism', []).
    directive('prism', [function () {
        return {
            restrict: 'A',
            link: function ($scope, element) {
                element.ready(function () {
                    if(element.find('code')[0]){
                        Prism.highlightElement(element.find('code')[0]);
                    }else {
                        Prism.highlightElement(element[0]);
                    }
                });
            }
        };
    }]);
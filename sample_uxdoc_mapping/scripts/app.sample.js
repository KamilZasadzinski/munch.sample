var NineYi;
(function (NineYi) {
    var Sample;
    (function (Sample) {
        angular.module('NineYi.Sample', [
            'ui.router',
            'ng.prism',
            'NineYi.FrontEnd.Munch',
            'NineYi.Sample.Controllers',
            'NineYi.Sample.Configs'
        ]);
        angular.module('NineYi.Sample.Controllers', ['NineYi.Sample.Services']);
        angular.module('NineYi.Sample.Configs', []);
        angular.module('NineYi.Sample.Services', []);
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
(function (NineYi) {
    var Sample;
    (function (Sample) {
        var Controllers;
        (function (Controllers) {
            var MenuController = (function () {
                function MenuController() {
                }
                MenuController.prototype.IsActive = function (state) {
                    return window.location.hash.indexOf(state) > 0;
                };
                return MenuController;
            }());
            MenuController.$inject = [];
            Controllers.MenuController = MenuController;
            angular.module('NineYi.Sample.Controllers')
                .controller('MenuController', MenuController);
        })(Controllers = Sample.Controllers || (Sample.Controllers = {}));
    })(Sample = NineYi.Sample || (NineYi.Sample = {}));
})(NineYi || (NineYi = {}));
//# sourceMappingURL=app.sample.js.map
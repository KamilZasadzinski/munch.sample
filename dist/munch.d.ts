﻿declare module munch.AddressEntity{
 interface CityEntity{
 Id: number;
 Title: string;
 Count: number;
 }
 interface AreaEntity {
 Id: number;
 CityId: number;
 Title: string;
 ZipCode: number;
 Count: number;
 }
}
declare module munch.DateTimeCombineRadioPickerDirectiveEntity{
 interface IDateTimeCombineRadioPickerDirectiveScope extends ng.IScope {
 fromDateString: string;
 fromDateObject: Date
 toDateString: string;
 toDateObject: Date;
 maxDate: Date;
 fromDateChanged: () => void;
 toDateChanged: () => void;
 isRequired:boolean;
 isDisabled:boolean;
 startName:string;
 endName:string;
 specificEndDate: string;
 queryType: string;
 dayAgo: number;
 }
}
declare module munch.DateTimePickerEntity{
 interface IDateTimePickerDirectiveScope extends ng.IScope {
 fromDateTimeString: string;
 toDateTimeString: string;
 fromDate:string;
 toDate:string;
 fromTime:string;
 toTime:string;
 maxDate: Date;
 minDate: Date;
 fromDateTimeChanged: () => void;
 toDateTimeChanged: () => void;
 isRequired:boolean;
 startDateName:string;
 endDateName:string;
 startTimeName:string;
 endTimeName:string;
 }
}
declare module munch.DateTimeRangePickerEntity {
 interface IDateTimeRangePickerDirectiveScope extends ng.IScope {
 fromDateString: string;
 fromDateObject: Date
 toDateString: string;
 toDateObject: Date;
 maxDate: Date;
 minDate: Date;
 fromDateChanged: () => void;
 toDateChanged: () => void;
 isRequired:boolean;
 isDisabled:boolean;
 startName:string;
 endName:string;
 }
}
declare module munch.DialogEntity{
 interface IDialogUtilityEntity{
 Notice: (message:string, title?:string,showCloseButton?:boolean) => ng.IPromise<string>;
 AutoCloseNotice: (message:string) => ng.IPromise<string>;
 Confirm: (message:string, AcceptMessage?:string, CancelMessage?:string, UseHtml?:boolean, showCancel?:boolean,isShortMessage?:boolean,title?:string, showCloseButton?:boolean) => ng.IPromise<string>
 Delete: (message:string, DeleteMessage?:string, UseHtml?:boolean, title?:string, showCancel?:boolean, showCloseButton?:boolean) => ng.IPromise<string>
 Custom: (opts: {
 message: string,
 acceptBtnText?: string,
 cancelBtnText?: string,
 useHtml?: boolean,
 title?: string,
 showCloseButton?:boolean,
 acceptEvent?: () => void,
 cancelEvent?: () => void
 }) => ng.IPromise<string>;
 NoticeFromServer: (message:string,isShortMessage?:boolean,title?:string,showCloseButton?:boolean) => ng.IPromise<string>;
 Redirect: (message:string, RedirectText:string, RedirectUrl:string,FinishMessage?:string, title?:string, showCloseButton?:boolean) => ng.IPromise<string>;
 RedirectReload: (message:string, RedirectText:string, RedirectUrl:string, FinishMessage?:string,title?:string, showCloseButton?:boolean) => ng.IPromise<string>;
 Close: () => void;
 }
}
declare module munch.GroupedTableHeaderActionEntity{
 interface IGroupedTableHeaderActionDirectiveScope extends ng.IScope {
 // 出貨使用的template 裝箱配送或整個宅配
 windowUrl: string;
 windowTitle: string;
 open:($event) => void;
 }
}
declare module munch.ImageMutipleUploadEntity {
 interface IImageMultipleUploadDirectiveScope extends ng.IScope {
 // 是否為並填欄位
 IsRequired:boolean;
 // 已選擇上傳的圖片
 ImageList: string[];
 // 上傳圖片群組
 Group: string;
 // UniqueKey
 UniqueKey: string;
 // 檔案最大上限
 MaxSize:number;
 // 檔案最小下限
 MinSize:number;
 // 最小寬度
 MinWidth:number;
 // 最小高度
 MinHeight:number;
 // 固定寬度
 FixWidth:number;
 // 固定高度
 FixHeight:number;
 // 是否通過驗證
 IsValid:boolean;
 // 上傳限制張數
 FilesLimit:number;
 // 上傳圖片queue計數
 AddedCounter:number;
 // 刪除圖片後 callback
 RemoveCallback: () => void;
 // 選擇要在手機預覽圖上顯示的圖片路徑
 SelectedImageRoot:string;
 // 圖片上傳API網址
 APIUrl:string;
 // 圖片上傳預覽API網址
 APIRenderTempUrl:string;
 }
}
declare module munch.ImageUploadEntity{
 interface IImageUploadDirectiveScope extends ng.IScope {
 // 是否為並填欄位
 IsRequired:boolean;
 // 已選擇上傳的圖片
 SelectedImage: string;
 // 上傳圖片群組
 Group: string;
 // UniqueKey
 UniqueKey: string;
 // FileName
 FileName: string;
 // 限制可上傳圖檔的副檔名
 FileType: string;
 // ImageUploadType
 Type: number;
 // ImageOperationType
 Operation: number;
 // 檔案最大上限
 MaxSize:number;
 // 檔案最小下限
 MinSize:number;
 // 最小寬度
 MinWidth:number;
 // 最小高度
 MinHeight:number;
 // 固定寬度
 FixWidth:number;
 // 固定高度
 FixHeight: number;
 // 刪除圖片後callback
 RemoveCallback: () => void;
 // 圖片上傳API網址
 APIUrl:string;
 // 圖片上傳預覽API網址
 APIRenderTempUrl:string;
 }
}
declare module munch.ImageUploadMobileEntity{
 interface IImageUploadMobileDirectiveScope extends ng.IScope {
 $flow:any;
 // 是否為並填欄位
 IsRequired:boolean;
 // 已選擇上傳的圖片
 // SelectedImage: string;
 //// 已選擇上傳的圖片
 ImageList: string[];
 // 上傳圖片群組
 Group: string;
 // UniqueKey
 UniqueKey: string;
 // FileName
 FileName: string;
 // ImageUploadType
 Type: number;
 // ImageOperationType
 Operation: number;
 // 檔案最大上限
 MaxSize:number;
 // 檔案最小下限
 MinSize:number;
 // 手機檔案最大上限，以手機拍照為準，過大不處理
 PhoneLimitSize:number;
 // 最小寬度
 MinWidth:number;
 // 最小高度
 MinHeight:number;
 // 最大寬度
 MaxWidth:number;
 // 最大高度
 MaxHeight:number;
 // 上傳限制張數
 FilesLimit:number;
 // 選擇要在手機預覽圖上顯示的圖片路徑
 SelectedImageRoot:string;
 // 圖片上傳API網址
 APIUrl:string;
 // 圖片上傳預覽API網址
 APIRenderTempUrl:string;
 }
}
declare module munch.LoadingEntity{
 interface ILoadingUtilityEntity{
 Start: () => void;
 Stop: () => void;
 }
}
declare module munch.ModalEntity{
 interface IModalUtilityEntity {
 ModalSizeOption: {
 Small: string;
 Medium: string;
 Large: string;
 };
 Form: (title:string, formTemplateUrl:string, confirmText:string, formItem:any, validateFunction:any, displayFooter?:boolean, windowClass?:string, showCloseButton?:boolean) => ng.IPromise<string>;
 Open: (title:string, templateUrl:string, CtrlName:string, scope:ng.IScope, size:string, windowClass?:string, showCloseButton?:boolean) => ng.IPromise<string>;
 Close: () => void;
 CloseAll: (reason?:any) => void;
 }
}
declare module munch.NotificationEntity{
 interface INotificationInfo {
 title?: string;
 message?: string;
 }

 interface INotificationEntity{
 positionEnum: any;
 afterNotificationActionEnum: any;
 position: any;
 afterAction: any;
 ShowNotify: (data: string|any, type: string, autoHideAfter?: number, position?: number, afterAction?:number, redirectHref?:string) => ng.IPromise<void>;
 HideNotify: () => ng.IPromise<void>;
 NotificationType: {
 Success: string;
 Warning: string;
 Info: string;
 Error: string;
 Custom: string;
 }
 }
}
declare module munch.RegexEntity{
 interface IRegexConfigEntity{
 ChineseAndEnglish: RegExp;
 Email: RegExp;
 NumberOnly: RegExp;
 NumberAndEnglish: RegExp;
 Phone: RegExp;
 Date: RegExp; // yyyy/MM/dd
 Time: RegExp; // 24hr format HH:MM or HH:MM:SS
 // login 名字包含中英文和符號
 NumberEnglishCharacter: RegExp;
 // 中文名字只能有中文
 ZhCharacter: RegExp;
 // 名稱可以包含中文 英文 數字 特殊符號
 ZhEnNumChar: RegExp;
 // 公司名稱包含中英文 數字
 ZhEnNumber: RegExp;
 // 免付費 domain 英文 數字 _ -
 DomainPattern: RegExp;
 // 官網 url 不需http://開頭
 URLPattern: RegExp;
 // 一般 Url http 或 https 開頭
 HttpURLPattern: RegExp;
 // 英文姓名 英文 & - 空格
 EngNamePattern: RegExp;
 // 電話號碼 數字 - #
 Telephone: RegExp;
 // 電話號碼 數字 0開頭 後面0-9皆可
 ServicePhone: RegExp;
 // 重設密碼的authCode是 數字 + 大小寫英文 組成，而且是在網址的最後(/之前)
 AuthCodePattern: RegExp;
 // 信用卡號 數字 空格
 NumberAndSpace: RegExp;
 }
}
declare module munch.TableEntity{
 interface ITableUtilityEntity{
 UpdateTableStatus: (tableState:any, PageSize:number, TotalCount:number) => ng.IPromise<void>;
 SortTable: <T>(tableState:any, array:T[]) => T[];
 }
}
declare module munch.TimeRangePickerEntity{
 interface ITimeRangePickerDirectiveScope extends ng.IScope {
 startTimeString:string;
 endTimeString:string;
 startTime: Date;
 endTime: Date;
 maxTime: Date;
 minTime: Date;
 startTimeChanged: () => void;
 endTimeChanged: () => void;
 isRequired:boolean;
 startTimeName:string;
 endTimeName:string;
 validPattern:RegExp;
 isSubmitted:boolean;
 }
}
declare module munch.ToastEntity{
 interface IToastUtilityEntity{
 Show: (message:string) => ng.IPromise<string>;
 Custom: (opts: {
 message: string,
 useHtml?: boolean,
 duration?:number
 }) => ng.IPromise<string>;

 }
}
declare module munch.UrlUtilityEntity{
 interface IUrlUtility{
 GetQuerystring: (name: string) => string;
 AppendParameter: (url: string, key: string, value: string, atStart?:boolean) => string;
 }
}
declare module munch.WindowUtilityEntity{
 interface IWindowUtility{
 WindowLeft:number;
 WindowTop:number;
 SettingWidth:number;
 SettingHeight:number;
 Open: (OpenUrl:string, Title:string, EnableRefleshParent?:boolean, SettingWidth?:number, SettingHeight?:number) => void
 Close: () => void;
 }
}
